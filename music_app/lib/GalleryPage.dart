import 'package:flutter/material.dart';
import 'package:audio_video_progress_bar/audio_video_progress_bar.dart';
import 'package:music_app/playerModelClass.dart';
import 'PlayerPage.dart';

class GalleryPage extends StatefulWidget {
  const GalleryPage({super.key});

  @override
  State createState() => _GalleryPageState();
}

class _GalleryPageState extends State {
  int currentIndex = 0;
  //bool player = false;
  bool playing = true;
 
  Widget playingScreen(bool playerScreen) {
  
    if (playerScreen == false) {
      print("in gallery");
      return Scaffold(
        backgroundColor: Colors.black87,
        body: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                width: 400,
                height: 400,
                //alignment: Alignment.topCenter,
                decoration: const BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage("assets/galleryImg.png"),
                        fit: BoxFit.fitWidth)),
                child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Spacer(),
                      const Text(
                        "A.L.O.N.E",
                        style: TextStyle(
                            fontSize: 32,
                            fontWeight: FontWeight.w700,
                            color: Colors.white),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Container(
                        height: 30,
                        width: 100,
                        decoration: BoxDecoration(
                            color: Colors.red,
                            borderRadius: BorderRadius.circular(20)),
                        child: const Center(
                          child: Text(
                            "Subscribe",
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.w800),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                    ],
                  ),
                ),
              ),
              SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Column(
                  children: [
                    const Padding(
                      padding: EdgeInsets.only(
                          left: 18.0, right: 18, top: 18, bottom: 10),
                      child: Row(
                        children: [
                          Text(
                            "Discography",
                            style: TextStyle(
                                color: Colors.red,
                                fontWeight: FontWeight.w700,
                                fontSize: 14),
                          ),
                          Spacer(),
                          Text(
                            "See all",
                            style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w600,
                                color: Colors.amber),
                          )
                        ],
                      ),
                    ),
                    SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Padding(
                        padding: const EdgeInsets.only(
                          left: 14,
                        ),
                        child: Row(
                          children: [
                            Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  InkWell(
                                    onTap: () {
                                      setState(() {
                                        PlayerpageState.playingScreenData(
                                            img: "assets/img1.png",
                                            album: "Dead Inside",
                                            artist: "kukroza",
                                            song: "Living");

                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    Playerpage()));
                                      });
                                    },
                                    child: Container(
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(18)),
                                      child: Image.asset(
                                        "assets/img1.png",
                                        height: 140,
                                      ),
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  const Text(
                                    "Dead Inside",
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w400,
                                        color: Colors.white),
                                  ),
                                  Text(
                                    "2020",
                                    style: TextStyle(
                                        fontSize: 10,
                                        fontWeight: FontWeight.w400,
                                        color: Colors.grey.shade500),
                                  )
                                ],
                              ),
                            ),
                            const SizedBox(
                              width: 20,
                            ),
                            Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  InkWell(
                                    highlightColor: Colors.grey.shade100,
                                    onTap: () {
                                      setState(() {
                                        PlayerpageState.playingScreenData(
                                            img: "assets/img2.png",
                                            album: "Alone",
                                            artist: "Jones",
                                            song: "Flying");

                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    Playerpage()));
                                      });
                                    },
                                    child: Container(
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(18)),
                                      child: Image.asset(
                                        "assets/img2.png",
                                        height: 140,
                                      ),
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  const Text(
                                    "Alone",
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w400,
                                        color: Colors.white),
                                  ),
                                  Text(
                                    "2022",
                                    style: TextStyle(
                                        fontSize: 10,
                                        fontWeight: FontWeight.w400,
                                        color: Colors.grey.shade500),
                                  )
                                ],
                              ),
                            ),
                            const SizedBox(
                              width: 20,
                            ),
                            Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  InkWell(
                                    onTap: () {
                                      setState(() {
                                        PlayerpageState.playingScreenData(
                                            img: "assets/img2.png",
                                            album: "album",
                                            artist: "artist",
                                            song: "song");

                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    Playerpage()));
                                      });
                                    },
                                    child: Container(
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(18)),
                                      child: InkWell(
                                        child: Image.asset(
                                          "assets/img1.png",
                                          height: 140,
                                        ),
                                      ),
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  const Text(
                                    "Dead Inside",
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w400,
                                        color: Colors.white),
                                  ),
                                  Text(
                                    "2020",
                                    style: TextStyle(
                                        fontSize: 10,
                                        fontWeight: FontWeight.w400,
                                        color: Colors.grey.shade500),
                                  )
                                ],
                              ),
                            ),
                            const SizedBox(
                              width: 20,
                            ),
                            Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  InkWell(
                                    onTap: () {
                                      setState(() {
                                        PlayerpageState.playingScreenData(
                                            img: "assets/img2.png",
                                            album: "youralbum",
                                            artist: "you",
                                            song: "dj oggy");

                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    Playerpage()));
                                      });
                                    },
                                    child: Container(
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(18)),
                                      child: Image.asset(
                                        "assets/img3.png",
                                        height: 140,
                                      ),
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  const Text(
                                    "Heartless",
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w400,
                                        color: Colors.white),
                                  ),
                                  Text(
                                    "2024",
                                    style: TextStyle(
                                        fontSize: 10,
                                        fontWeight: FontWeight.w400,
                                        color: Colors.grey.shade500),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    const Padding(
                      padding: EdgeInsets.only(
                          left: 18.0, right: 18, top: 18, bottom: 10),
                      child: Row(
                        children: [
                          Text(
                            "Popular Singles",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w700,
                                fontSize: 14),
                          ),
                          Spacer(),
                          Text(
                            "See all",
                            style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w600,
                                color: Colors.amber),
                          )
                        ],
                      ),
                    ),
                    Column(
                      children: [
                        InkWell(
                          onTap: () {
                            setState(() {
                              PlayerpageState.playingScreenData(
                                  img: "assets/img4.png",
                                  album: "Easy Living",
                                  artist: "Drake",
                                  song: "We Are Chaos");

                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Playerpage()));
                            });
                          },
                          child: Row(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 18.0, right: 18),
                                child: Image.asset(
                                  "assets/img4.png",
                                  height: 80,
                                ),
                              ),
                              const Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "We Are Chaos",
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400,
                                        color: Colors.white),
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        "2021 ",
                                        style: TextStyle(
                                            fontSize: 10,
                                            fontWeight: FontWeight.w400,
                                            color: Colors.grey),
                                      ),
                                      Icon(
                                        Icons.circle,
                                        color: Colors.white,
                                        size: 4,
                                      ),
                                      Text(
                                        " Easy Living",
                                        style: TextStyle(
                                            fontSize: 10,
                                            fontWeight: FontWeight.w400,
                                            color: Colors.grey),
                                      )
                                    ],
                                  )
                                ],
                              ),
                              const Spacer(),
                              const Padding(
                                padding: EdgeInsets.all(18.0),
                                child: Icon(
                                  Icons.scatter_plot_outlined,
                                  color: Colors.white,
                                  size: 20,
                                ),
                              )
                            ],
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        InkWell(
                          onTap: () {
                            setState(() {
                              PlayerpageState.playingScreenData(
                                  img: "assets/img5.png",
                                  album: "Happy Place",
                                  artist: "Jack",
                                  song: "Smile");

                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Playerpage()));
                            });
                          },
                          child: Row(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 18.0, right: 18),
                                child: Image.asset(
                                  "assets/img5.png",
                                  height: 80,
                                ),
                              ),
                              const Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Smile",
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400,
                                        color: Colors.white),
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        "2023 ",
                                        style: TextStyle(
                                            fontSize: 10,
                                            fontWeight: FontWeight.w400,
                                            color: Colors.grey),
                                      ),
                                      Icon(
                                        Icons.circle,
                                        color: Colors.white,
                                        size: 4,
                                      ),
                                      Text(
                                        " Happy Place",
                                        style: TextStyle(
                                            fontSize: 10,
                                            fontWeight: FontWeight.w400,
                                            color: Colors.grey),
                                      )
                                    ],
                                  )
                                ],
                              ),
                              const Spacer(),
                              const Padding(
                                padding: EdgeInsets.all(18.0),
                                child: Icon(
                                  Icons.scatter_plot_outlined,
                                  color: Colors.white,
                                  size: 20,
                                ),
                              )
                            ],
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        InkWell(
                          onTap: () {
                            setState(() {
                              PlayerpageState.playingScreenData(
                                  img: "assets/galleryImg.png",
                                  album: "Art",
                                  artist: "Simon",
                                  song: "Alone");

                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Playerpage()));
                            });
                          },
                          child: Row(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 18.0, right: 18),
                                child: InkWell(
                                  child: Container(
                                    // height: 50,
                                    // width: 47,
                                    decoration: BoxDecoration(
                                        //color: Colors.amber,
                                        borderRadius: BorderRadius.circular(0)),
                                    child: Image.asset(
                                      "assets/galleryImg.png",
                                      height: 80,
                                      width: 75,
                                    ),
                                  ),
                                ),
                              ),
                              const Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Alone",
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400,
                                        color: Colors.white),
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        "2021 ",
                                        style: TextStyle(
                                            fontSize: 10,
                                            fontWeight: FontWeight.w400,
                                            color: Colors.grey),
                                      ),
                                      Icon(
                                        Icons.circle,
                                        color: Colors.white,
                                        size: 4,
                                      ),
                                      Text(
                                        " Art",
                                        style: TextStyle(
                                            fontSize: 10,
                                            fontWeight: FontWeight.w400,
                                            color: Colors.grey),
                                      )
                                    ],
                                  )
                                ],
                              ),
                              const Spacer(),
                              const Padding(
                                padding: EdgeInsets.all(18.0),
                                child: Icon(
                                  Icons.scatter_plot_outlined,
                                  color: Colors.white,
                                  size: 20,
                                ),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      );
    } else {
      return Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black87,
      body: playingScreen(false),
      bottomNavigationBar: NavigationBar(
        backgroundColor: Colors.black,
        height: 60,
        onDestinationSelected: (int index) {
          setState(() {
            currentIndex = index;
          });
        },
        selectedIndex: currentIndex,
        destinations: const [
          NavigationDestination(
              icon: Icon(
                Icons.home_rounded,
                size: 20,
                color: Colors.blueGrey,
              ),
              selectedIcon: Icon(Icons.home_rounded),
              label: "Home"),
          NavigationDestination(
              icon: Icon(
                Icons.search_rounded,
                size: 20,
                color: Colors.blueGrey,
              ),
              label: "Search"),

          // NavigationDestination(
          //     icon: Icon(
          //       Icons.shopping_cart_rounded,
          //       size: 20,
          //       color: Colors.blueGrey,
          //     ),
          //     label: "Cart"),
          NavigationDestination(
              icon: Icon(
                Icons.favorite_border_rounded,
                size: 20,
                color: Colors.blueGrey,
              ),
              label: "Favorite"),
          NavigationDestination(
              icon: Icon(
                Icons.account_circle_rounded,
                size: 20,
                color: Colors.blueGrey,
              ),
              label: "Profile"),
        ],
      ),
    );
  }
}
