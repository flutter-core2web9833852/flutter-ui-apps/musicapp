import 'package:flutter/material.dart';
import 'package:music_app/GalleryPage.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State createState() => _HomePageState();
}

class _HomePageState extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(201, 11, 11, 11),
      body: SafeArea(
        child: Expanded(
          child: Center(
            child: Column(
              children: [
                //Spacer(),
                Container(
                  height: 700,
                  decoration: const BoxDecoration(
                      image: DecorationImage(
                    image: AssetImage("assets/homeImg.png"),
                    scale: 0.9,
                    fit: BoxFit.fitHeight,
                  )),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      const Padding(
                        padding: EdgeInsets.all(20.0),
                        child: Text(
                          "Dancing between The shadows Of rhythm ",
                          style: TextStyle(
                              fontSize: 34,
                              fontWeight: FontWeight.w700,
                              color: Colors.white),
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      InkWell(
                        highlightColor: Colors.black,
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => GalleryPage()));
                        },
                        child: Container(
                          alignment: Alignment.center,
                          height: 40,
                          width: 200,
                          decoration: BoxDecoration(
                              color: Colors.red,
                              borderRadius: BorderRadius.circular(20)),
                          child: const Text(
                            "Get Started",
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.w600),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Container(
                  alignment: Alignment.center,
                  height: 40,
                  width: 200,
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.red),
                      borderRadius: BorderRadius.circular(20)),
                  child: const Text("Continue with Email",
                      style: TextStyle(
                          color: Colors.red,
                          fontSize: 16,
                          fontWeight: FontWeight.w600)),
                ),
                const Padding(
                  padding: EdgeInsets.only(left: 80, right: 80, top: 10),
                  child: Text(
                    "by continuing you agree to terms of services and  Privacy policy",
                    style: TextStyle(
                        fontSize: 12,
                        color: Color.fromARGB(255, 65, 65, 65),
                        fontWeight: FontWeight.w400),
                  ),
                ),
                Spacer()
              ],
            ),
          ),
        ),
      ),
    );
  }
}
