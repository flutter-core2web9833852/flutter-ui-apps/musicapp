import 'package:flutter/material.dart';
import 'package:music_app/GalleryPage.dart';
import 'playerModelClass.dart';
import 'package:audio_video_progress_bar/audio_video_progress_bar.dart';

class Playerpage extends StatefulWidget {
  const Playerpage({super.key});

  @override
  State createState() => PlayerpageState();
}

class PlayerpageState extends State {
  bool playing = true;

  static PlayerData? playerObj;
  static playingScreenData({String? album, String? img, String? artist, String? song}) {
    
    playerObj = PlayerData(img: "assets/img2.png", album: "a", artist: "a", song: "a");
    //print("playing screen data");
    // print(album);
    if (album != null) {
      playerObj!.album = album;
      //print("object");
      //print(playerObj!.album);
    }
    if (img != null) {
      playerObj!.img = img;
    }
    if (artist != null) {
      playerObj!.artist = artist;
    }
    if (song != null) {
      playerObj!.song = song;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black87,
      body: Center(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 20, top: 40),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  IconButton(
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const GalleryPage()));
                      },
                      icon: const Icon(
                        Icons.arrow_back_ios_new_rounded,
                        color: Colors.white,
                      )),
                ],
              ),
            ),
            Container(
              height: 500,
              width: 400,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage(playerObj!.img), scale: 0.1)),
            ),
            Column(
              children: [
                // const Spacer(),
                // const SizedBox(
                //   height: 20,
                // ),
                Text(
                  playerObj!.album,
                  style: const TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.w400,
                      color: Colors.amber),
                ),
                Text(
                  playerObj!.artist,
                  style: const TextStyle(
                      fontSize: 26,
                      fontWeight: FontWeight.w400,
                      color: Colors.white),
                ),
                const SizedBox(
                  height: 20,
                )
              ],
            ),
            Text(
              playerObj!.song,
              style: const TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w400,
                  color: Colors.amber),
            ),
            const SizedBox(
              height: 5,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 30, right: 30, bottom: 10),
              child: ProgressBar(
                progress: const Duration(milliseconds: 1000),
                //buffered: Duration(milliseconds: 2000),
                total: const Duration(milliseconds: 60000),
                barHeight: 4,
                baseBarColor: Colors.grey.shade900,
                bufferedBarColor: Colors.grey.shade800,
                progressBarColor: Colors.amber,
                barCapShape: BarCapShape.round,
                thumbRadius: 0,
                thumbColor: Colors.amberAccent,
                thumbGlowColor: Colors.amber,
                thumbGlowRadius: 6,
                timeLabelLocation: TimeLabelLocation.sides,
                timeLabelTextStyle:
                    const TextStyle(fontSize: 16, color: Colors.white70),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 40.0, right: 40),
              child: Row(
                children: [
                  const Icon(
                    Icons.replay,
                    color: Colors.white,
                    size: 28,
                  ),
                  const Spacer(),
                  const Icon(
                    Icons.arrow_back_ios_new_rounded,
                    color: Colors.white,
                    size: 28,
                  ),
                  const Spacer(),
                  IconButton(
                    onPressed: () {
                      setState(() {
                        playing = !playing;
                      });
                    },
                    icon: playing
                        ? const Icon(
                            Icons.play_circle_fill_rounded,
                            color: Colors.white,
                            size: 50,
                          )
                        : const Icon(
                            Icons.pause_circle_filled_rounded,
                            color: Colors.white,
                            size: 50,
                          ),
                  ),
                  const Spacer(),
                  const Icon(
                    Icons.arrow_forward_ios,
                    color: Colors.white,
                    size: 28,
                  ),
                  const Spacer(),
                  const Icon(
                    Icons.volume_up_rounded,
                    color: Colors.white,
                    size: 28,
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 0,
            ),
          ],
        ),
      ),
    );
  }
}
